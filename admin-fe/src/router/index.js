import Vue from 'vue'
import VueRouter from 'vue-router'

import Index from '@/pages/Index'
import Login from '@/pages/Login'

Vue.use(VueRouter)

const router = new VueRouter({
  routes: [{
    name: 'Index',
    path: '/',
    component: Index,
  },{
    name: 'Login',
    path: '/login',
    component: Login,
  }, {
    path: '*',
    redirect: '/',
  }],

  mode: 'history',
})

export default router
