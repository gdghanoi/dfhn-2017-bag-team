import firebase from 'firebase'

const firebaseApp = firebase.initializeApp({
  apiKey: "AIzaSyDQOk7J06jvE2rB0En_dUyjmSGD2_AokJM",
  authDomain: "bag-team-restaurant.firebaseapp.com",
  databaseURL: "https://bag-team-restaurant.firebaseio.com",
  projectId: "bag-team-restaurant",
  storageBucket: "bag-team-restaurant.appspot.com",
  messagingSenderId: "601182684727",
})

const db = firebaseApp.database()

export {
  db,
}
