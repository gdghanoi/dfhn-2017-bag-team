import store from '@/store'

export default {
  beforeRouteEnter(to, from, next) {
    if (to.name === 'Login' || store.state.user.isAuthenticated) {
      next()
    } else {
      next({ name: 'Login' })
    }
  },
}
