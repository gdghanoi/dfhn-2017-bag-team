// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import 'onsenui/css/onsenui.css'
import 'onsenui/css/onsen-css-components.css'

import Vue from 'vue'
import VueOnsen from 'vue-onsenui'
import Vuefire from 'vuefire'

import './firebase'
import store from './store'
import router from './router'
import App from './App'
import checkAuthen from './mixins/checkAuthen'

Vue.config.productionTip = false

Vue.use(VueOnsen)
Vue.use(Vuefire)

Vue.mixin(checkAuthen)

/* eslint-disable no-new */
new Vue({
  store,
  router,
  ...App,
}).$mount('#app')
