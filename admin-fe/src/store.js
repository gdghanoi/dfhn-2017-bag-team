import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    user: {
      isAuthenticated: false,
    },
  },

  mutations: {
    setUserIsAuthenticated(state, value) {
      Vue.set(state.user, 'isAuthenticated', value)
    },
  }
})
