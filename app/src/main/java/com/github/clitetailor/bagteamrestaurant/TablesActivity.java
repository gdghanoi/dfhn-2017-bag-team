package com.github.clitetailor.bagteamrestaurant;

import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Layout;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.PopupWindow;

import com.firebase.ui.auth.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.zip.Inflater;

import static android.view.Gravity.CENTER;
import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;
import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

public class TablesActivity extends AppCompatActivity {

    private DatabaseReference mDatabase;
    private FirebaseAuth auth = FirebaseAuth.getInstance();
    private FirebaseUser user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tables);

        mDatabase = FirebaseDatabase.getInstance().getReference();

        user = auth.getCurrentUser();
        user.getUid();

        Log.d(":::", "created");

        GridLayout gridLayout = findViewById(R.id.tables_layout);

        for (Integer i = 0; i < 10; ++i) {
            Button button = new Button(this);

            button.setText(i.toString());

            View view = button;
            GridLayout.LayoutParams layoutParams = new GridLayout.LayoutParams();
            layoutParams.width = WRAP_CONTENT;
            layoutParams.height = WRAP_CONTENT;
            layoutParams.setGravity(CENTER);

            button.setLayoutParams(layoutParams);

            /**  End of table button.  **/

            gridLayout.addView(button);
        }

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ArrayList<DataSnapshot> tableDataSnapshots = (ArrayList<DataSnapshot>) dataSnapshot.child("tables").getChildren();

                for (Integer i = 0; i < tableDataSnapshots.size(); ++i) {
                    Table table = tableDataSnapshots.get(i).getValue(Table.class);
                    Log.d(":::", table.floor);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void popupWindow(Integer i) {
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.table_info_form, null);

        final PopupWindow pWindow = new PopupWindow(this);
        pWindow.setContentView(popupView);

        pWindow.showAtLocation(new LinearLayout(this), Gravity.CENTER, 0, 10);

        popupView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pWindow.dismiss();
            }
        });
    }
}
