package com.github.clitetailor.bagteamrestaurant;

/**
 * Created by ducnh on 18-Nov-17.
 */

public class Table {
    public Integer nSeats;
    public Boolean available;
    public String floor;
    public String note;
    public String uuid;

    public Table(
            Integer nSeats,
            Boolean available,
            String floor,
            String note,
            String uuid
    ) {
        this.nSeats = nSeats;
        this.available = available;
        this.floor = floor;
        this.note = note;
        this.uuid = uuid;
    }
}
