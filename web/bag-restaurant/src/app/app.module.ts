import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { TablesPageComponent } from './tables-page/tables-page.component';
import { LoginComponent } from './login/login.component';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabaseModule, AngularFireDatabase } from 'angularfire2/database';
import { environment } from './environment';
import { AuthService } from './auth.service';

const appRoutes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'tables', component: TablesPageComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    TablesPageComponent,
    LoginComponent
  ],
  imports: [
    AngularFireModule.initializeApp(environment.firebase, 'bag-team-restaurant'),
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    AngularFireAuthModule,
    AngularFireDatabaseModule
  ],
  providers: [
    AuthService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
