import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase/app';
import { AngularFireDatabase } from 'angularfire2/database';

@Component({
  selector: 'app-tables-page',
  templateUrl: './tables-page.component.html',
  styleUrls: ['./tables-page.component.styl']
})
export class TablesPageComponent implements OnInit {

  tables = [];

  constructor(
    private db: AngularFireDatabase
  ) {
    this.db.database.ref('tables').on('value', (tables) => {
      this.tables = tables.val();
    });
  }

  ngOnInit() {
  }
}
