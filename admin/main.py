import uuid
import random
import os
import time

import firebase_admin
from firebase_admin import credentials
from firebase_admin import db

from flask import Flask, jsonify, request, redirect, url_for, send_file, make_response
from flask_cors import CORS, cross_origin

# Setup app
# Firebase
cred = credentials.Certificate('key.json')
default_app = firebase_admin.initialize_app(cred, {
    'databaseURL': 'https://bag-team-restaurant.firebaseio.com'
})

# Flask
app = Flask(__name__)
CORS(app)


# APIs
@app.route('/')
def pong():
    return 'Pong\n', {'Content-Type': 'text-plain; charset=utf-8'}

@app.route('/tables')
def list_tables():
    ref = db.reference('tables')
    tables = ref.get()
    return jsonify(tables)

@app.route('/tables/<table_id>')
def table_info(table_id):
    ref = db.reference('tables/{}'.format(table_id))
    return jsonify(ref.get())

@app.route('/bookings')
def list_bookings():
    """List pending bookings"""
    ref = db.reference('bookings')
    bookings = ref.get()
    pendings = [booking for booking in bookings if booking['status'] == 'pending']
    return jsonify(pendings)

@app.route('/bookings/history')
def list_bookings_history():
    """List bookings not pending"""
    ref = db.reference('bookings')
    bookings = ref.get()
    bookings_history = [booking for booking in bookings if booking['status'] != 'pending']
    return jsonify(bookings_history)

@app.route('/bookings/<booking_id>')
def booking_info(booking_id):
    ref = db.reference('bookings/{}'.format(booking_id))
    return jsonify(ref.get())

@app.route('/bookings/<booking_id>/<status>', methods=['PATCH'])
def update_booking(booking_id, status):
    """Update status of booking"""
    ref = db.reference('bookings/{}'.format(booking_id))
    ref.update({'statsus': status})
    return jsonify({
        'success': True,
        'code': 200})


if __name__ == "__main__":
    app.run()
