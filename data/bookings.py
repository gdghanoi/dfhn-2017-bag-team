import firebase_admin
from firebase_admin import credentials
from firebase_admin import db

import uuid
import datetime
import random


if __name__ == "__main__":
    cred = credentials.Certificate('key.json')
    default_app = firebase_admin.initialize_app(cred, {
        'databaseURL': 'https://bag-team-restaurant.firebaseio.com'
    })

    tables = db.reference('tables').get()

    ref = db.reference('bookings')
    bookings = {}
    for i in range(9):
        bookings[i] = {
                'uuid': str(uuid.uuid4()),
                'checkin': datetime.datetime.now().isoformat(),
                'status': random.choice(['pending', 'canceled', 'approved', 'declined']),
                'table_id': random.randrange(len(tables)),
                'user_id': random.choice(['HmRj147MWvekhqRsTIq7gCEOjs73', '4veQYsEBYmeADCKnY8bmTzZhTPH3'])}
    ref.set(bookings)
