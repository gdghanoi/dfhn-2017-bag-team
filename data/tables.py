import firebase_admin
from firebase_admin import credentials
from firebase_admin import db

import uuid
import random


if __name__ == "__main__":
    cred = credentials.Certificate('key.json')
    default_app = firebase_admin.initialize_app(cred, {
        'databaseURL': 'https://bag-team-restaurant.firebaseio.com'
    })

    ref = db.reference('tables')
    tables = {}
    j = -1
    for i in range(9):
        if (i+1) % 3 == 0:
            j = j + 1
        tables[i] = {
                'floor': ['A', 'B', 'C'][j],
                'uuid': str(uuid.uuid4()),
                'avail': False}
    ref.set(tables)
